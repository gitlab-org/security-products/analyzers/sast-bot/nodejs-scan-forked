package main

import (
	"bytes"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"syscall"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/nodejs-scan/v2/metadata"
	"gitlab.com/gitlab-org/security-products/analyzers/ruleset/v3"
)

const (
	maxMsgLen          = 150
	njsscanConfig      = ".njsscan"
	reportPath         = "/tmp/njsscan.json"
	njsscanEmptyReport = `
{
  "errors": [],
  "nodejs": {},
  "templates": {}
}
`
)

func analyzeFlags() []cli.Flag {
	return []cli.Flag{}
}

func analyze(c *cli.Context, projectPath string, rulesetConfig *ruleset.Config) (io.ReadCloser, error) {
	configFile, err := loadRuleset(rulesetConfig)
	if err != nil {
		return nil, err
	}

	cmd := exec.Command("njsscan", "--config", configFile, "--json", "--output", reportPath, projectPath)
	output, err := cmd.CombinedOutput()
	log.Debugf("%s\n%s", cmd.String(), output)

	if exitErr, ok := err.(*exec.ExitError); ok {
		if exitErr.Sys().(syscall.WaitStatus).ExitStatus() == 1 {
			return os.Open(reportPath)
		}
		return nil, exitErr
	}

	// no vulnerabilities found so return an empty njsscan report
	return ioutil.NopCloser(bytes.NewReader([]byte(njsscanEmptyReport))), nil
}

func loadRulesetConfig(projectPath string) (*ruleset.Config, error) {
	// Load custom config if available
	rulesetPath := filepath.Join(projectPath, ruleset.PathSAST)
	return ruleset.Load(rulesetPath, metadata.AnalyzerID, log.StandardLogger())
}
