package main

import (
	"gitlab.com/gitlab-org/security-products/analyzers/ruleset/v3"

	log "github.com/sirupsen/logrus"
)

func loadRuleset(rulesetConfig *ruleset.Config) (string, error) {
	// Load custom ruleset if available
	if rulesetConfig != nil && len(rulesetConfig.Passthrough) != 0 {
		// Due to an issue in ruleset, ProcessPassthroughs returns an incorrect path if rulesetConfig.Path is not reset to "".
		// This is tested in https://gitlab.com/gitlab-org/security-products/analyzers/nodejs-scan/-/blob/68fe11883c415b7b6872619b5f3eebcab32c850f/spec/nodejs_scan_image_spec.rb#L104
		rulesetConfig.Path = ""
		path, err := ruleset.ProcessPassthroughs(rulesetConfig, log.StandardLogger())
		return path, err
	}

	return njsscanConfig, nil
}
