# NodeJsScan analyzer changelog

## v4.1.13
- Fix issue where remote rulesets are being ignored (!166)
- Update `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.9` => [`v3.0.2`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v3.0.2)] (!166)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command/v2` version [`v2.4.0` => [`v3.0.0`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v3.0.0)] (!166)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.4.0` => [`v5.1.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v5.1.0)] (!166)


## v4.1.12
- upgrade `github.com/urfave/cli/v2` version [`v2.27.1` => [`v2.27.2`](https://github.com/urfave/cli/releases/tag/v2.27.2)] (!164)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command/v2` version [`v2.2.0` => [`v2.4.0`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v2.4.0)] (!164)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.3.2` => [`v4.4.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.4.0)] (!164)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.8` => [`v2.0.9`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.9)] (!164)

## v4.1.11
- upgrade [`njsscan`](https://github.com/ajinabraham/njsscan) version [`0.3.6` => [`0.3.7`](https://github.com/ajinabraham/njsscan/releases/tag/0.3.7)] (!162)

## v4.1.10
- upgrade `github.com/stretchr/testify` version [`v1.8.4` => [`v1.9.0`](https://github.com/stretchr/testify/releases/tag/v1.9.0)] (!160)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.7` => [`v2.0.8`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.8)] (!160)

## v4.1.9
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.6` => [`v2.0.7`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.7)] (!159)

## v4.1.8
- upgrade `github.com/urfave/cli/v2` version [`v2.26.0` => [`v2.27.1`](https://github.com/urfave/cli/releases/tag/v2.27.1)] (!158)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.3.1` => [`v4.3.2`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.3.2)] (!158)

## v4.1.7
- upgrade `github.com/urfave/cli/v2` version [`v2.25.7` => [`v2.26.0`](https://github.com/urfave/cli/releases/tag/v2.26.0)] (!157)

## v4.1.6
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.2.0` => [`v4.3.1`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.3.1)] (!156)

## v4.1.5
- upgrade [`njsscan`](https://github.com/ajinabraham/njsscan) version [`0.3.4` => [`0.3.6`](https://github.com/ajinabraham/njsscan/releases/tag/0.3.6)] (!154)
    - Performance Improvement from libsast bump
    - Upgrade semgrep + libsast
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/common/v3` version [`v3.2.2` => [`v3.2.3`](https://gitlab.com/gitlab-org/security-products/analyzers/common/-/releases/v3.2.3)] (!154)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.1.5` => [`v4.2.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.2.0)] (!154)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.4` => [`v2.0.6`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.6)] (!154)

## v4.1.4
- upgrade `gitlab.com/gitlab-org/security-products/post-analyzers/tracking-calculator` to [`v2.4.1`](https://gitlab.com/gitlab-org/security-products/post-analyzers/tracking-calculator/-/releases/v2.4.1) (!152)

## v4.1.3
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.1.3` => [`v4.1.5`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.1.5)] (!150)
- Remove `title` field from `vulnerabilities` in report JSON (!150)

## v4.1.2
- upgrade `gitlab.com/gitlab-org/security-products/post-analyzers/tracking-calculator` to [`v2.3.8`](https://gitlab.com/gitlab-org/security-products/post-analyzers/tracking-calculator/-/releases/v2.3.8) (!149)

## v4.1.1
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.3` => [`v2.0.4`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.4)] (!147)
    - Update passthrough support to handle ambiguous/short refs

## v4.1.0
- Add [`tracking-calculator`](https://gitlab.com/gitlab-org/security-products/post-analyzers/tracking-calculator/) (!144)

## v4.0.4
- upgrade `github.com/urfave/cli/v2` version [`v2.25.5` => [`v2.25.7`](https://github.com/urfave/cli/releases/tag/v2.25.7)] (!141)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.1.2` => [`v4.1.3`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.1.3)] (!141)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.2` => [`v2.0.3`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.3)] (!141)
- Replace  deprecated `vulnerability.Message` with `vulnerability.Title` when building report (!141)
- Replace `gitlab.com/gitlab-org/security-products/analyzers/ruleset` with `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` (!141)

## v4.0.3
- upgrade `github.com/sirupsen/logrus` version [`v1.9.0` => [`v1.9.3`](https://github.com/sirupsen/logrus/releases/tag/v1.9.3)] (gitlab-org/security-products/analyzers/nodejs-scan!140)
- upgrade `github.com/stretchr/testify` version [`v1.8.2` => [`v1.8.4`](https://github.com/stretchr/testify/releases/tag/v1.8.4)] (gitlab-org/security-products/analyzers/nodejs-scan!140)
- upgrade `github.com/urfave/cli/v2` version [`v2.25.3` => [`v2.25.5`](https://github.com/urfave/cli/releases/tag/v2.25.5)] (gitlab-org/security-products/analyzers/nodejs-scan!140)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command/v2` version [`v2.1.0` => [`v2.2.0`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v2.2.0)] (gitlab-org/security-products/analyzers/nodejs-scan!140)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.1.0` => [`v4.1.2`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.1.2)] (gitlab-org/security-products/analyzers/nodejs-scan!140)

## v4.0.2
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v1.4.1` => [`v2.0.2`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.2)] (!139)

## v4.0.1
- upgrade `github.com/urfave/cli/v2` version [`v2.25.1` => [`v2.25.3`](https://github.com/urfave/cli/releases/tag/v2.25.3)] (!138)

## v4.0.0
- Bump to next major version (!136)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command/v2` version [`v1.10.2` => [`v2.1.0`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v2.1.0)] (!136)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v3.22.1` => [`v4.1.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.1.0)] (!136)

## v3.4.6
- upgrade `github.com/stretchr/testify` version [`v1.8.1` => [`v1.8.2`](https://github.com/stretchr/testify/releases/tag/v1.8.2)] (!135)
- upgrade `github.com/urfave/cli/v2` version [`v2.24.3` => [`v2.25.1`](https://github.com/urfave/cli/releases/tag/v2.25.1)] (!135)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.10.1` => [`v1.10.2`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.10.2)] (!135)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.17.0` => [`v3.22.1`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.22.1)] (!135)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset` version [`v1.4.0` => [`v1.4.1`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v1.4.1)] (!135)
- upgrade Go version to v1.19 (!135)

## v3.4.5
- Add upgrade all packages to the latest version (!134)

## v3.4.4
- upgrade `github.com/urfave/cli/v2` version [`v2.23.7` => [`v2.24.3`](https://github.com/urfave/cli/releases/tag/v2.24.3)] (!130)

## v3.4.3
- upgrade `github.com/urfave/cli/v2` version [`v2.23.6` => [`v2.23.7`](https://github.com/urfave/cli/releases/tag/v2.23.7)] (!129)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.10.0` => [`v1.10.1`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.10.1)] (!129)

## v3.4.2
- Log `njsscan` errors at the WARN level (!128)

## v3.4.1
- upgrade `github.com/urfave/cli/v2` version [`v2.23.5` => [`v2.23.6`](https://github.com/urfave/cli/releases/tag/v2.23.6)] (!127)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.16.0` => [`v3.17.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.17.0)] (!127)

## v3.4.0
- upgrade [`njsscan`](https://github.com/ajinabraham/njsscan) version [`0.3.3` => [`0.3.4`](https://github.com/ajinabraham/njsscan/releases/tag/0.3.4)] (!126)
- upgrade `github.com/stretchr/testify` version [`v1.8.0` => [`v1.8.1`](https://github.com/stretchr/testify/releases/tag/v1.8.1)] (!126)
- upgrade `github.com/urfave/cli/v2` version [`v2.16.3` => [`v2.23.5`](https://github.com/urfave/cli/releases/tag/v2.23.5)] (!126)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.9.1` => [`v1.10.0`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.10.0)] (!126)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/common/v3` version [`v3.2.1` => [`v3.2.2`](https://gitlab.com/gitlab-org/security-products/analyzers/common/-/releases/v3.2.2)] (!126)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.13.0` => [`v3.16.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.16.0)] (!126)

## v3.3.2
- Update common to `v3.2.1` to fix gotestsum cmd (!124)

## v3.3.1
- Fixed report error message key to display the error correctly (!123)

## v3.3.0
- upgrade `github.com/urfave/cli/v2` version [`v2.11.1` => [`v2.16.3`](https://github.com/urfave/cli/releases/tag/v2.16.3)] (!122)

## v3.2.0
- upgrade [`njsscan`](https://github.com/ajinabraham/njsscan) version [`0.3.1` => [`0.3.3`](https://github.com/ajinabraham/njsscan/releases/tag/0.3.3)] (!121)
- upgrade `github.com/sirupsen/logrus` version [`v1.8.1` => [`v1.9.0`](https://github.com/sirupsen/logrus/releases/tag/v1.9.0)] (!121)
- upgrade `github.com/stretchr/testify` version [`v1.7.0` => [`v1.8.0`](https://github.com/stretchr/testify/releases/tag/v1.8.0)] (!121)
- upgrade `github.com/urfave/cli/v2` version [`v2.11.0` => [`v2.11.1`](https://github.com/urfave/cli/releases/tag/v2.11.1)] (!121)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.8.2` => [`v1.9.1`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.9.1)] (!121)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.12.2` => [`v3.13.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.13.0)] (!121)

## v3.1.2
- Upgrade the `command` package for better analyzer messages (!120)

## v3.1.1
- Downgrade `libsast` to 1.5.0 (!119)
    - Prevents the analyzer from crashing when scanning repositories that contain symlinks

## v3.1.0
- Upgrade core analyzer dependencies (!118)
  + Adds support for globstar patterns when excluding paths
  + Adds analyzer details to the scan report

## v3.0.0
- Update to next major version, `v3.0.0` (!116)

## v2.23.1
- Pin ruamel.yaml to fix broken build (!115)

## v2.23.0
- Update ruleset, report, and command modules to support ruleset overrides (!113)

## v2.22.0
- Update njsscan to [v0.3.1](https://github.com/ajinabraham/njsscan/releases/tag/0.3.1) (!111)
    - Major libsast upgrade
    - Standard mapping support from libsast
    - Publish latest docker images from master and release
    - Performance Improvements
    - Major semgrep upgrade
    - SQLi rule bug fix
    - Rules QA
    - Support ES6 syntax for NoSQL find injection rule (@CharlyJazz)
    - Added Severity Filter (@ansidorov)
    - Remove Duplicated Rule
    - Refactor Tests

## v2.21.0
- Change base docker image from `python:3.7-alpine` to `python:3.10-alpine` (!110 @rpandini_wh)

## v2.20.0
- Bump ruleset module (!108)

## v2.19.3
- Update common to `v2.24.1` which fixes a git certificate error when using `ADDITIONAL_CA_CERT_BUNDLE` (!109)

## v2.19.2
- chore: Update go to v1.17 (!107)

## v2.19.1
- chore: Use ruleset.ProcessPassthrough helper (!106)

## v2.19.0
- Update njsscan to [v0.2.8](https://github.com/ajinabraham/njsscan/releases/tag/0.2.8) (!104)
    - Support njsscan-ignore for templates
    - deprecate ignore:
    - semgrep update
    - CWE Typo Fix
    - libsast pattern matcher to support ignore findings.

## v2.18.0
- Update njsscan to [v0.2.6](https://github.com/ajinabraham/njsscan/releases/tag/0.2.6) (!98)
    - License Change: LGPL2.1 -> LGPL3.0+
    - Semgrep bump
    - Support HTML output format

## v2.17.0
- Update njsscan to [v0.2.4](https://github.com/ajinabraham/njsscan/releases/tag/0.2.4) (!95)
    - Bump Semgrep version to 0.45
    - Update Max Scan file size from 25 to 5 MB.
    - Added New Sequelize Rules from Semgrep


## v2.16.0
- Update report dependency in order to use the report schema version 14.0.0 (!95)

## v2.15.0
- Update njsscan to [v0.2.3](https://github.com/ajinabraham/njsscan/releases/tag/0.2.3) (!94)
    - Skip files > 25MB

## v2.14.0
- Update njsscan to [v0.2.2](https://github.com/ajinabraham/njsscan/releases/tag/0.2.2) (!91)
    - New Rule Express hbs Local File Read
    - Rule QA
    - New config --config to support .njsscan file from a custom location
    - Replaced expires rule and maxAge rule

## v2.13.1
- Update common to `v2.22.1` which fixes a CA Certificate bug when analyzer is run more than once (!89)

## v2.13.0
- Update njsscan to v0.1.9 (!88)
  - Added rule to detect CWE-89 (SQL Injection) with knex.js.
  - Added rule to detect CWE-327 (broken or risky crypto algorithm) when AES is used without an initialization vector.

## v2.12.1
- Fix `scanner.id` to `nodejs-scan` (!87)

## v2.12.0
- Update common to v2.22.0 (!86)
- Update urfave/cli to v2.3.0 (!86)

## v2.11.0
- Update njsscan to v0.1.8 (!84)
- Update logrus, cli golang dependencies

## v2.10.1
- Update common library to fix a null bug, path bug, and add custom errors (!83)

## v2.10.0
- Update analyzer to use [njsscan](https://github.com/ajinabraham/njsscan) (!80)
- Replaces old [rules](https://github.com/ajinabraham/nodejsscan/blob/v3.7/core/rules.xml) with [100+ new rules](https://github.com/ajinabraham/njsscan/tree/master/njsscan/rules) based on semgrep. (!80)
- Changes base Docker image from `node:14-alpine3.12` to `python:3.7-alpine` (!80)

## v2.10.0
- Update common library to include rule disablement (!82)

## v2.9.6
- Warn if no files match instead of returning error (!79)

## v2.9.5
- Fix bug which prevented writing `ADDITIONAL_CA_CERT_BUNDLE` value to `/etc/gitconfig` (!77)

## v2.9.4
- Update common library and golang version (!76)

## v2.9.3
- Update node to v14.11 (!75)

## v2.9.2
- Update node and supporting dependencies (!72)

## v2.9.1
- Update node and supporting dependencies (!67)

## v2.9.0
- Add `scan.start_time`, `scan.end_time` and `scan.status` to report (!70)

## v2.8.1
- Upgrade go to version 1.15 (!69)

## v2.8.0
- Add scan object to report (!64)

## v2.7.2
- Add `app.Version` to support `scan.scanner.version` field in security reports (!63)

## v2.7.1
- Bump JS dependencies to latest versions (!61)

## v2.7.0
- Switch to the MIT Expat license (!60)

## v2.6.0
- Add `SAST_DISABLE_BABEL` flag (!58)

## v2.5.0
- Update logging to be standardized across analyzers (!59)

## v2.4.1
- Remove `location.dependency` from the generated SAST report (!56)

## v2.4.0
- Update third-party dependencies to latest versions (!40)

## v2.3.0
- Add `id` field to vulnerabilities in JSON report (!31)

## v2.2.0
- Add support for custom CA certs (!27)

## v2.1.1
- Allow babel to run from anywhere in the FS, to fix a bug in non-DinD mode (!19)

## v2.1.0
- Upgrade Babel from 6 to 7 (!20)

## v2.0.2
- Set full default path for `rules.xml` to avoid error when running without Docker-in-Docker (!17)

## v2.0.1
- Update common to v2.1.6

## v2.0.0
- Switch to new report syntax with `version` field

## v1.2.0
- Add an `Identifier` generated from the NodeJsScan's rule name

## v1.1.0
- Add `Scanner` property and deprecate `Tool`

## v1.0.0
- Initial release
