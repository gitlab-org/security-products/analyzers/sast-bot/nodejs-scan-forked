package metadata_test

import (
	"reflect"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/nodejs-scan/v2/metadata"
	report "gitlab.com/gitlab-org/security-products/analyzers/report/v5"
)

func TestReportScanner(t *testing.T) {
	want := report.ScannerDetails{
		ID:      "njsscan",
		Name:    "njsscan",
		Version: metadata.ScannerVersion,
		Vendor: report.Vendor{
			Name: "GitLab",
		},
		URL: "https://github.com/ajinabraham/njsscan",
	}
	got := metadata.ReportScanner

	if !reflect.DeepEqual(want, got) {
		t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
	}
}
