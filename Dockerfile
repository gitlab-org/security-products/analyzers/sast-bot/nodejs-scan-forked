ARG POST_ANALYZER_SCRIPTS_VERSION=0.2.0
ARG TRACKING_CALCULATOR_VERSION=2.4.1

FROM registry.gitlab.com/security-products/post-analyzers/scripts:${POST_ANALYZER_SCRIPTS_VERSION} AS scripts
FROM registry.gitlab.com/security-products/post-analyzers/tracking-calculator:${TRACKING_CALCULATOR_VERSION} AS tracking

FROM golang:1.19-alpine AS build

ENV CGO_ENABLED=0 GOOS=linux
WORKDIR /go/src/app
COPY . .
# build the analyzer binary and automatically set the AnalyzerVersion
# variable to the most recent version from the CHANGELOG.md file
RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
        PATH_TO_MODULE=`go list -m` && \
        go build -ldflags="-X '$PATH_TO_MODULE/metadata.AnalyzerVersion=$CHANGELOG_VERSION'" -o analyzer

# Install njsscan
FROM python:3.10-alpine

ARG SCANNER_VERSION
ENV SCANNER_VERSION ${SCANNER_VERSION:-0.3.7}

RUN pip install ruamel.yaml==0.16.12 njsscan==$SCANNER_VERSION

# Downgrade libsast to 1.5.0 so the scanner works against directories containing
# symlinks. See https://gitlab.com/gitlab-org/gitlab/-/issues/369062
RUN pip install libsast==1.5.0

RUN apk --no-cache -U upgrade && \
    apk --no-cache add git ca-certificates gcc libc-dev

COPY --chown=root:root --from=build /go/src/app/analyzer /analyzer-binary
COPY .njsscan .njsscan
COPY --from=tracking /analyzer-tracking /analyzer-tracking
COPY --from=scripts /start.sh /analyzer

ENTRYPOINT []
CMD ["/analyzer", "run"]
