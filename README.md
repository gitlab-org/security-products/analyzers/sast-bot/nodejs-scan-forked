### [Maintenance Notice](https://docs.gitlab.com/ee/update/deprecations#sast-analyzer-consolidation-and-cicd-template-changes):
This analyzer is currently in terminal maintenance mode. No new major versions will be released. Please see our [semgrep](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep) analyzer for NodeJsScan rules.

# NodeJsScan analyzer

NodeJsScan performs SAST scanning on repositories containing code written in the following language: `javascript`.

The analyzer wraps [njsscan](https://github.com/ajinabraham/njsscan), a tool that checks NodeJS code for CWEs based on [semgrep](https://github.com/returntocorp/semgrep) rules, and is written in Go. It's structured similarly to other Static Analysis analyzers because it uses the shared [command](https://gitlab.com/gitlab-org/security-products/analyzers/command) package.

The analyzer is built and published as a Docker image in the GitLab Container Registry associated with this repository. You would typically use this analyzer in the context of a [SAST](https://docs.gitlab.com/ee/user/application_security/sast) job in your CI/CD pipeline. However, if you're contributing to the analyzer or you need to debug a problem, you can run, debug, and test locally using Docker.

For instructions on local development, please refer to the [README in Analyzer Scripts](https://gitlab.com/gitlab-org/secure/tools/analyzer-scripts/-/blob/master/analyzers-common-readme.md).

## Updating the underlying scanner

Check if there is an updated version of [njsscan](https://pypi.org/project/njsscan). If there is, update the version in the Dockerfile with the desired version:

```dockerfile
RUN pip install njsscan=={desired njsscan version}
```

## Versioning and release process

Please check the [versioning and release process documentation](https://gitlab.com/gitlab-org/security-products/analyzers/common#versioning-and-release-process).

## Contributing

Contributions are welcome, see [`CONTRIBUTING.md`](CONTRIBUTING.md) for more details.

## License

This code is distributed under the MIT Expat license, see the [LICENSE](LICENSE) file.
